package booking_protocol;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Booking
 */
public class Booking extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String PARAMETER = "state";
	public enum BookingStates {
	    NEW,
	    PAYMENT,
	    COMPLETED
	}

    public Booking() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleRequest(request, response);
	}

	private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		Object sessionState = session.getAttribute(PARAMETER);
		BookingStates currentState = (BookingStates) sessionState;
		BookingStates requestedState;
		String parameterValue;
        
		if (currentState == null) {
			request.getSession().setAttribute(PARAMETER, BookingStates.NEW);
			out.append("Your state is NEW.").println();
			return;
		}
		
		parameterValue = request.getParameter(PARAMETER);
		if (parameterValue == null || parameterValue.isEmpty()) {
			out.append("Parameter " + PARAMETER + " is empty!").println();
			return;
		}
		
		try {
			requestedState = BookingStates.valueOf(parameterValue);
		} catch (IllegalArgumentException e) {
			out.append("Invalid state!").println();
			response.setStatus(400);
			return;
		}

		if (requestedState.ordinal() != currentState.ordinal() + 1) {
			out.append("Invalid state! Your state is " + currentState.name() + ".");
		} else {
			out.append("OK, your state is " + requestedState.name() + ".");
			if (requestedState.name().equals("COMPLETED")) {
				request.getSession().removeAttribute(PARAMETER);
				out.append(" Transaction is done.");
			} else {
				request.getSession().setAttribute(PARAMETER, requestedState);
			}
		}
	}	
}
